{
  description = "A NixOS flake for the Astralship and the machines aboard it.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, ... }@inputs: {
    nixosConfigurations = {
      fu1 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          (import ./hosts/fu-cluster/configuration.nix)
          (import ./hosts/fu-cluster/fu1/configuration.nix)
        ];
        specialArgs = { inherit inputs; };
      };
      fu2 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          (import ./hosts/fu-cluster/configuration.nix)
          (import ./hosts/fu-cluster/fu2/configuration.nix)
        ];
        specialArgs = { inherit inputs; };
      };
      fu3 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          (import ./hosts/fu-cluster/configuration.nix)
          (import ./hosts/fu-cluster/fu3/configuration.nix)
        ];
        specialArgs = { inherit inputs; };
      };
    };
  };
}
